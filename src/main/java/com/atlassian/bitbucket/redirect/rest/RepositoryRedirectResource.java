package com.atlassian.bitbucket.redirect.rest;

import com.atlassian.bitbucket.redirect.RepositoryRedirectDao;
import com.atlassian.bitbucket.rest.util.ResponseFactory;
import com.atlassian.bitbucket.rest.util.RestUtils;
import com.atlassian.bitbucket.permission.Permission;
import com.atlassian.bitbucket.permission.PermissionService;
import com.sun.jersey.spi.resource.Singleton;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.HashMap;
import java.util.Map;

@Path("redirects")
@Singleton
@Consumes({MediaType.APPLICATION_JSON})
@Produces({RestUtils.APPLICATION_JSON_UTF8})
public class RepositoryRedirectResource {

    private final RepositoryRedirectDao repositoryRedirectDao;
    private final PermissionService permissionService;

    public RepositoryRedirectResource(RepositoryRedirectDao repositoryRedirectDao, PermissionService permissionService) {
        this.repositoryRedirectDao = repositoryRedirectDao;
        this.permissionService = permissionService;
    }

    @PUT
    @Path("{repositoryId}")
    public Response put(@PathParam("repositoryId") Integer repositoryId,
                        @QueryParam("redirect") String redirectUrl) {
        if (!checkAdminPermission(repositoryId)) {
            return ResponseFactory.status(Response.Status.FORBIDDEN).build();
        }

        if (repositoryId == null || redirectUrl == null) {
            return ResponseFactory.status(Response.Status.BAD_REQUEST)
                    .entity("A numeric repositoryId path parameter and a String 'redirect' query parameter are required.").build();
        }

        repositoryRedirectDao.setRedirect(repositoryId, redirectUrl);

        return ResponseFactory.noContent().build();
    }

    @DELETE
    @Path("{repositoryId}")
    public Response delete(@PathParam("repositoryId") Integer repositoryId) {
        if (!checkAdminPermission(repositoryId)) {
            return ResponseFactory.status(Response.Status.FORBIDDEN).build();
        }

        if (repositoryId == null) {
            return ResponseFactory.status(Response.Status.BAD_REQUEST)
                    .entity("A numeric repositoryId path parameter is required.").build();
        }

        repositoryRedirectDao.removeRedirect(repositoryId);

        return ResponseFactory.noContent().build();
    }

    @GET
    public Response get() {
        Map<Integer, String> redirects = repositoryRedirectDao.getRedirects();

        Map<Integer, String> filtered = new HashMap<Integer, String>();
        for (Map.Entry<Integer, String> entry : redirects.entrySet()) {
            if (checkAdminPermission(entry.getKey())) {
                filtered.put(entry.getKey(), entry.getValue());
            }
        }

        return ResponseFactory.ok(filtered).build();
    }

    @GET
    @Path("{repositoryId}")
    public Response get(@PathParam("repositoryId") Integer repositoryId) {
        if (!checkAdminPermission(repositoryId)) {
            return ResponseFactory.status(Response.Status.FORBIDDEN).build();
        }

        if (repositoryId == null) {
            return ResponseFactory.status(Response.Status.BAD_REQUEST)
                    .entity("A numeric repositoryId path parameter is required.").build();
        }

        String redirect = repositoryRedirectDao.getRedirectUrl(repositoryId);
        if (redirect == null) {
            return ResponseFactory.status(Response.Status.NOT_FOUND).build();
        }
        Map<Integer, String> response = new HashMap<Integer, String>();
        response.put(repositoryId, redirect);

        return ResponseFactory.ok(response).build();
    }

    private boolean checkAdminPermission(Integer repositoryId) {
        return permissionService.hasRepositoryPermission(repositoryId, Permission.REPO_ADMIN);
    }

}
