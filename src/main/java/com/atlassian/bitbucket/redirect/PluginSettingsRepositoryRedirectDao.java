package com.atlassian.bitbucket.redirect;

import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;

import java.util.HashMap;
import java.util.Map;

public class PluginSettingsRepositoryRedirectDao implements RepositoryRedirectDao {

    private static final String PLUGIN_SETTINGS_KEY = "repository.redirect.map";

    private final PluginSettingsFactory pluginSettingsFactory;

    public PluginSettingsRepositoryRedirectDao(PluginSettingsFactory pluginSettingsFactory) {
        this.pluginSettingsFactory = pluginSettingsFactory;
    }

    @Override
    public String getRedirectUrl(Integer repositoryId) {
        Map<String, String> redirects = getRedirectMap();

        if (redirects == null) {
            return null;
        }

        return redirects.get(String.valueOf(repositoryId));
    }

    @Override
    public void setRedirect(Integer repositoryId, String url) {
        set(repositoryId, url);
    }

    @Override
    public void removeRedirect(Integer repositoryId) {
        set(repositoryId, null);
    }

    @Override
    public Map<Integer, String> getRedirects() {
        Map<Integer, String> transformed = new HashMap<Integer, String>();
        Map<String, String> stored = getRedirectMap();
        if (stored != null) {
            for (Map.Entry<String, String> entry : stored.entrySet()) {
                transformed.put(Integer.parseInt(entry.getKey()), entry.getValue());
            }
        }
        return transformed;
    }

    private synchronized void set(Integer repositoryId, String url) {
        Map<String, String> redirectMap = getRedirectMap();
        if (redirectMap == null) {
            redirectMap = new HashMap<String, String>();
        }

        if (url == null) {
            redirectMap.remove(String.valueOf(repositoryId));
        } else {
            redirectMap.put(String.valueOf(repositoryId), url);
        }

        pluginSettingsFactory.createGlobalSettings().put(PLUGIN_SETTINGS_KEY, redirectMap);
    }

    /**
     * @return the redirect map, or {@code null} if no redirects have been persisted yet
     */
    @SuppressWarnings("unchecked")
    private Map<String, String> getRedirectMap() {
        return (Map<String, String>)
                pluginSettingsFactory.createGlobalSettings().get(PLUGIN_SETTINGS_KEY);
    }

}
