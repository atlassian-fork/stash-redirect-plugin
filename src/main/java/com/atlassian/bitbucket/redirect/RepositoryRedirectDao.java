package com.atlassian.bitbucket.redirect;

import java.util.Map;

public interface RepositoryRedirectDao {

    String getRedirectUrl(Integer repositoryId);

    void setRedirect(Integer repositoryId, String url);

    void removeRedirect(Integer repositoryId);

    Map<Integer, String> getRedirects();

}
